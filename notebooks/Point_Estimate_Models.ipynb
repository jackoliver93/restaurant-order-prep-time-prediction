{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "960ad1a6-87d7-41cc-97fa-46852ec8e4c0",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Modelling Food Preparation Times\n",
    "\n",
    "### Approach\n",
    "I took a simple approach to modelling in order to understand what is acheivable using standard methods, by which I mean:\n",
    " * Ignorning the hierarchical structure of the data, and developing a single predictive model for all restaurants. \n",
    " * Including restaurent level features at the order level, i.e. performing a merge between orders and restaurants data sets. \n",
    " * Considering a handful of simple regression models using the scikit-learn package. \n",
    "\n",
    "At the end of the notebook I will conclude with the pros and cons of this approach, and talk about what I would explore further, given more time. \n",
    "\n",
    "## Set-up"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13d8b862-4ed3-487b-91f9-313dcedd1431",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from scipy.stats import ks_2samp\n",
    "from sklearn.preprocessing import FunctionTransformer, OneHotEncoder\n",
    "from sklearn.compose import ColumnTransformer\n",
    "from sklearn.pipeline import Pipeline\n",
    "from sklearn.model_selection import train_test_split, GridSearchCV\n",
    "from sklearn.metrics import mean_squared_error\n",
    "\n",
    "from sklearn.dummy import DummyRegressor\n",
    "from sklearn.linear_model import LinearRegression\n",
    "from sklearn.ensemble import RandomForestRegressor\n",
    "from sklearn.neighbors import KNeighborsRegressor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e762e14e-7d4d-4a0d-bdde-80291f513b64",
   "metadata": {},
   "outputs": [],
   "source": [
    "def filter_rows(df: pd.DataFrame, n_hours: int = 10) -> pd.DataFrame:\n",
    "    \"\"\"Filters out orders with prep time of 0 and those above n_hours hours.\n",
    "    \"\"\"\n",
    "    return df[(df[\"prep_time_seconds\"]!=0) & (df[\"prep_time_seconds\"]<n_hours*3600)].reset_index(drop=True)\n",
    "\n",
    "def pool_ordinal(\n",
    "    df: pd.DataFrame,\n",
    "    column_name: str = \"number_of_items\",\n",
    "    threshold: int = 10\n",
    ") -> np.array:\n",
    "    \"\"\"Pools all values of an integer, ordinal DataFrame column that\n",
    "    are above a certain value, threshold, to the value threshold.\n",
    "    \"\"\"\n",
    "    df.loc[df[column_name]>threshold]=threshold\n",
    "    return df[column_name].values.reshape(-1, 1)\n",
    "    \n",
    "def pool_rare_values(\n",
    "    df: pd.DataFrame, \n",
    "    variable_name: str, \n",
    "    threshold: float = 0.001, \n",
    "    pooled_value: str = \"other\"\n",
    ") -> pd.Series:\n",
    "    \"\"\"Pools values of a discrete variable that have a relative frequency below\n",
    "    a chosen threshold, which is 0.05 by default. The pooling maps all rare values\n",
    "    onto a string, which is set to the string, 'other', by default.\n",
    "    \"\"\"\n",
    "    frequencies = df[variable_name].value_counts(normalize=True)\n",
    "    rare = frequencies[frequencies<threshold]\n",
    "    mapping = {key: pooled_value for key in rare.index}\n",
    "    return df[variable_name].map(lambda x: mapping.get(x, x))\n",
    "\n",
    "def create_datetime(\n",
    "    df: pd.DataFrame,\n",
    "    dt_col_name: str = \"order_acknowledged_at\",\n",
    "    format: str = \"%Y-%m-%d %H:%M:%S.%f\"\n",
    ") -> pd.Series:\n",
    "    \"\"\"Creates a Series of type datetime, from a series of string representations of datetimes.\n",
    "    \"\"\"\n",
    "    return pd.to_datetime(df[dt_col_name], format=format, utc=True)\n",
    "\n",
    "def create_hour_variable(df: pd.DataFrame) -> np.array:\n",
    "    \"\"\"Encodes the hour of an order as a binary variable (0 when hour<12, 1 when hour>=12).\n",
    "    \"\"\"    \n",
    "    hours = create_datetime(df=df).dt.hour.values         \n",
    "    hours_encoded = np.zeros(len(df))\n",
    "    hours_encoded[np.where(hours>11)] = 1\n",
    "    return hours_encoded.reshape(-1, 1)  # reshape because ColumnTransformer wants a 2D output\n",
    "\n",
    "def pool_rare_values(\n",
    "    df: pd.DataFrame, \n",
    "    variable_name: str, \n",
    "    threshold: float = 0.05, \n",
    "    pooled_value: str = \"other\"\n",
    ") -> np.array:\n",
    "    \"\"\"Pools values of a discrete variable that have a relative frequency below\n",
    "    a chosen threshold, which is 0.05 by default. The pooling maps all rare values\n",
    "    onto a string, which is set to 'other' by default.\n",
    "    \"\"\"\n",
    "    frequencies = df[variable_name].value_counts(normalize=True)\n",
    "    rare = frequencies[frequencies<threshold]\n",
    "    mapping = {key: pooled_value for key in rare.index}\n",
    "    return df[variable_name].map(lambda x: mapping.get(x, x)).values\n",
    "                                        \n",
    "# Create the transformer objects and build pipeline\n",
    "row_transformer = FunctionTransformer(filter_rows)\n",
    "ordinal_transformer = FunctionTransformer(pool_ordinal)\n",
    "time_transformer = FunctionTransformer(create_hour_variable)\n",
    "categorical_transformer = FunctionTransformer(pool_rare_values)\n",
    "continuous_transformer = FunctionTransformer(np.log1p, inverse_func=np.expm1)\n",
    "\n",
    "col_transformer = ColumnTransformer(\n",
    "    transformers=[\n",
    "        (\"time\", time_transformer, [\"order_acknowledged_at\"]),\n",
    "        (\"ordinal\", ordinal_transformer, [\"number_of_items\"]),\n",
    "        (\"num\", continuous_transformer, [\"order_value_gbp\"]),\n",
    "        (\"identity\", FunctionTransformer(), \n",
    "             [\n",
    "                 'city_Paris',\n",
    "                 'city_other',\n",
    "                 'type_of_food_burgers',\n",
    "                 'type_of_food_indian',\n",
    "                 'type_of_food_italian',\n",
    "                 'type_of_food_other',\n",
    "                 'type_of_food_thai',\n",
    "            ]\n",
    "        )\n",
    "    ],\n",
    "    remainder=\"drop\"\n",
    ")\n",
    "\n",
    "# The final features appear in order that they occur in col_transformer.\n",
    "# I will manually record their names. \n",
    "predictor_names = [\n",
    "    \"order_acknowledged_at\",\n",
    "    \"number_of_items\",\n",
    "    \"log_order_value_gbp\",\n",
    "    \"city_Paris\",\n",
    "    \"city_other\",\n",
    "    \"type_of_food_burgers\",\n",
    "    \"type_of_food_indian\",\n",
    "    \"type_of_food_italian\",\n",
    "    \"type_of_food_other\",\n",
    "    \"type_of_food_thai\",\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b75d503-72b8-4a65-82bb-01724467a1f6",
   "metadata": {},
   "outputs": [],
   "source": [
    "orders = pd.read_csv(\"../data/orders.csv.gz\")\n",
    "restaurants = pd.read_csv(\"../data/restaurants.csv.gz\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61e9d1d2-98ed-446e-a406-bcc5682f8879",
   "metadata": {},
   "source": [
    "## 1- Prepare features\n",
    "I will develop scikit-learn pipelines to transform and select features that were decided upon in the EDA notebook.  \n",
    " * I want to transform the order data and the restaurant data separately, so I have more flexibility about how I use/combine them.\n",
    " * I will transform the target variable separately, becuase it makes it easier to apply the inverse transformation the transformation after prediction. \n",
    " \n",
    " To recap, for the order data we want to: \n",
    "\n",
    "* Filter values based on the prep time.\n",
    "* Create a new binary variable derived from *order_acknowledged_at*.\n",
    "* Pool all values of *number_of_items* greater than 10 to the integer 10.\n",
    "* Perform a $\\log(1+x)$ transform *order_value_gbp*.\n",
    "* Drop all other columns. \n",
    "\n",
    "For the restaurant data I want to:\n",
    " * Pool values of *city* and *type_of_food* that have a relative frequency that is lower than 0.05.\n",
    " * One-hot encode the resulting pooled variables. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bec12739-3c31-4520-9704-02ab5520818d",
   "metadata": {},
   "outputs": [],
   "source": [
    "restaurants_reduced = restaurants.loc[:, [\n",
    "    \"restaurant_id\", \n",
    "    \"city\",\n",
    "    \"type_of_food\",\n",
    "]]\n",
    "\n",
    "restaurants_reduced.loc[:, \"city\"] = pool_rare_values(restaurants, \"city\", threshold=0.05)\n",
    "restaurants_reduced.loc[:, \"type_of_food\"] = pool_rare_values(restaurants, \"type_of_food\", threshold=0.05)\n",
    "\n",
    "restaurants_reduced = pd.get_dummies(\n",
    "    restaurants_reduced, columns=[\"city\", \"type_of_food\"], drop_first=True\n",
    ")\n",
    "\n",
    "data = orders.merge(restaurants_reduced, left_on=\"restaurant_id\", right_on=\"restaurant_id\")\n",
    "\n",
    "# manually filter rows so I can keep the target variable out of col_transformer\n",
    "data = filter_rows(data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3c6d9b69-7604-45ed-91b7-88a78cf17f7c",
   "metadata": {},
   "outputs": [],
   "source": [
    "target = \"prep_time_seconds\"\n",
    "features = [c for c in list(data) if c!=target]\n",
    "X = data[features]\n",
    "y = data[target]\n",
    "\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)\n",
    "\n",
    "X_train_trans = col_transformer.fit_transform(X_train)\n",
    "X_test_trans = col_transformer.transform(X_test)\n",
    "y_train_trans = continuous_transformer.fit_transform(y_train.values.reshape(-1, 1))\n",
    "y_test_trans = continuous_transformer.fit_transform(y_test.values.reshape(-1, 1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a8b7736-ac22-40fd-b5f0-19b436b174e7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Quickly check that the target variable has similar distibution in the train test split\n",
    "statistic, p_value = ks_2samp(y_train, y_test)\n",
    "print(statistic, p_value)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "508ef4be-b51d-44db-8f03-c409ed7b9241",
   "metadata": {},
   "source": [
    "A Kolmogorov–Smirnov test tells us that the target distributions are mostly the same for train and test sets."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "83e272af-dd8e-44bd-acd0-a00bf78f627b",
   "metadata": {},
   "source": [
    "## 2- Obtain a baseline by predicting the mean"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c074124b-99a7-4427-a2b7-6d109491d9d7",
   "metadata": {},
   "outputs": [],
   "source": [
    "regressor = DummyRegressor()\n",
    "\n",
    "# Fit on one column at a time to double check DummyRegressor gives the same results, regardless\n",
    "column_to_fit = 2\n",
    "regressor.fit(X_train_trans[:, column_to_fit].reshape(-1, 1), y_train_trans.reshape(-1, 1))\n",
    "\n",
    "y_train_pred_1 = regressor.predict(X_train_trans[:, column_to_fit].reshape(-1, 1))\n",
    "y_test_pred_1 = regressor.predict(X_test_trans[:, column_to_fit].reshape(-1, 1))\n",
    "\n",
    "y_train_pred_inv_1 = continuous_transformer.inverse_transform(y_train_pred_1.reshape(-1, 1))\n",
    "y_test_pred_inv_1 = continuous_transformer.inverse_transform(y_test_pred_1.reshape(-1, 1))\n",
    "\n",
    "dummy_train_rmse = mean_squared_error(y_train, y_train_pred_inv_1, squared=False)\n",
    "dummy_test_rmse = mean_squared_error(y_test, y_test_pred_inv_1, squared=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1aadc405-5a47-474b-a923-fbd1baa23fc1",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f\"Baseline train RMSE by prediciting the mean = {round(dummy_train_rmse, 4)} seconds.\")\n",
    "print(f\"Baseline test RMSE by prediciting the mean = {round(dummy_test_rmse, 4)} seconds.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe7d2243-21f0-45e0-a7ed-f2adb3e34a63",
   "metadata": {},
   "source": [
    "### Comments\n",
    "The RMSE score to beat is 1426 seconds, or approximately 24 minutes, which we can obtain by simply predicting the mean of the training set. Note that the RMSE score is larger for the training set than the test set. This indicates that there is more variance in the training set target values."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d926c4a5-3383-4ee2-93e5-c32aa0091b64",
   "metadata": {},
   "source": [
    "## 3- Modelling with order-level and restaurant-level features\n",
    "I will test a few regression classes from scikit-learn with a limited range of hyperparameter settings, since the goal is to show methodology, as opposed to find incremental performance increases. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7fcff41-736b-4092-9280-f3e054f7488b",
   "metadata": {},
   "outputs": [],
   "source": [
    "estimator = Pipeline([(\"estimator\", LinearRegression())])\n",
    "\n",
    "param_grid = [\n",
    "    {\n",
    "        'estimator': [LinearRegression()],\n",
    "    },\n",
    "    {\n",
    "        \"estimator\": [RandomForestRegressor(n_estimators=200)],\n",
    "        \"estimator__max_depth\": [3, 5, 10, 12],\n",
    "    },\n",
    "    {\n",
    "     \"estimator\": [KNeighborsRegressor()],\n",
    "     \"estimator__n_neighbors\": [100, 150, 200]\n",
    "    }\n",
    "]\n",
    "\n",
    "# Because I need to inverse the transformation on the predictions before interpreting\n",
    "# the RMSE, I will score the grid_search using the explained variance.\n",
    "\n",
    "model = GridSearchCV(\n",
    "    estimator=estimator, param_grid=param_grid, scoring=\"explained_variance\",\n",
    "    cv=10, n_jobs=-1, return_train_score=True\n",
    ")\n",
    "model.fit(X_train_trans, y_train_trans.ravel());"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a000fb7-9963-4e58-b592-1940b36b266c",
   "metadata": {},
   "outputs": [],
   "source": [
    "cv_results = pd.DataFrame(model.cv_results_)\n",
    "cv_results = pd.DataFrame(model.cv_results_).sort_values(by=\"rank_test_score\")\n",
    "cv_results.loc[:, \"overfitting\"] = cv_results.loc[:, \"mean_train_score\"] - cv_results.loc[:, \"mean_test_score\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cd6b56d0-7a6e-4973-a191-e25bd3c5a166",
   "metadata": {},
   "outputs": [],
   "source": [
    "cv_results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16324bd2-4461-4e76-bd2a-fd4ed3d28512",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig,ax= plt.subplots(figsize=(8,12))\n",
    "df_plot = cv_results.set_index('params')\n",
    "df_plot[\"mean_test_score\"].plot(\n",
    "    kind='bar',\n",
    "    yerr=df_plot[\"std_test_score\"],\n",
    "    facecolor='lightblue',\n",
    "    ax=ax\n",
    ")\n",
    "plt.ylabel(\"Mean Test Explained Variance \",fontsize=14)\n",
    "plt.xlabel(\"Regression model\",fontsize=14)\n",
    "plt.xticks(rotation=90, horizontalalignment=\"center\")\n",
    "\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bd89754a-08b8-4d5c-a1af-c80394273357",
   "metadata": {},
   "outputs": [],
   "source": [
    "cv_results.head(10).loc[:, [\"mean_test_score\", \"mean_train_score\", \"overfitting\"]]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0dc99ddd-9bb1-434b-a254-17947b517731",
   "metadata": {},
   "source": [
    "### Analysis of the grid search results\n",
    "Although the RandomForestRegressor with max_depth=10, had the highest mean test score, it shows overfitting, which is reflected by the standard deviation of the test scores (black line plotted on bar chart). Reducing the max_depth hyperparameter reduces the amount of overfitting, however the LinearRegression model explains a comparible amount of the variance in the data with far less overfitting than a RandomForest model with max_depth set to both 10 and 5. \n",
    "\n",
    "Reducing the number of features considered when bootstrapping each dataset in the ensemble may be another way to reduce overfitting, but for now I'll proceed with analysing the Linear Regression model, since I'd expect this to be more reliable in a production environment, given the above results. \n",
    "\n",
    "The next step is to train a new linear regression model and evaluate the RMSE score on the test set. \n",
    "\n",
    "## Evaluating a Linear Regression Model on the test set"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "06fc44d7-2fcc-4c29-8d85-ab2fe34794e8",
   "metadata": {},
   "outputs": [],
   "source": [
    "lr = LinearRegression()\n",
    "lr.fit(X_train_trans, y_train_trans.ravel())\n",
    "\n",
    "predictions_trans = lr.predict(X_test_trans).reshape(-1, 1)\n",
    "predictions = continuous_transformer.inverse_transform(predictions_trans)\n",
    "rmse = mean_squared_error(y_test, predictions, squared=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e7205b22-9972-4a26-8e33-307f2bdd0ff8",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f\"Test RMSE of a linear regression model = {round(rmse, 4)} seconds.\")\n",
    "lr_uplift = dummy_test_rmse - rmse\n",
    "print(f\"A Linear Regression model improved test RMSE by {round(lr_uplift, 4)} seconds.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "268bd803-6e9f-44b3-a0fc-c712e70d4a46",
   "metadata": {},
   "source": [
    "### Comments\n",
    "The results seem promising, showing that a simple regression model can improve the RMSE by almost 2 minutes, corresponding to a percentage decrease of the RMSE by 7.9%. Before talking about the pros and cons of this approach, I want to look at the model coefficients to try to understand if there are redundancies in my features (in terms of offering a predictive boost), and the residuals, to understand how their variance changes depending on the predicted value.\n",
    "\n",
    "### Examining the model coefficients\n",
    "This is useful because it helps confirm any redundancies which we may want to remove in future modelling iterations, and may help inform us about where to focus when engineering new features, or which data to gather, to introduce new features into the dataset. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2dd34955-40f9-4f37-aefb-9e55b6284c8a",
   "metadata": {},
   "outputs": [],
   "source": [
    "coefficients = pd.DataFrame(list(zip(predictor_names, lr.coef_)), columns=[\"coef\", \"value\"])\n",
    "coefficients.sort_values(by=\"value\", ascending=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "71e2ac63-99f1-4f02-b766-89d64919e927",
   "metadata": {},
   "source": [
    "The coefficients reflect what we expect from the EDA phase, which gives me further confidence in the model, in terms of how general it is.\n",
    "\n",
    "* There is a positive correlation between *order_value_gbp* and *prep_time_seconds*. Since both variables were subject to the same transformation we can say that an increase in order value of £1.00 leads to an increase in the prep time by 0.49 seconds. Remember that there was some correlation between *order_value_gbp* and *number_of_items*, which may effect the parameter estimate (it does slightly, but I've not shown the analysis here. After removing *number_of_items* and re-running the notebook, the coefficient changed to 0.50).\n",
    "\n",
    "* *number_of_items* has the lowest coefficient and this was anticipated after oberving a small Spearman's rank coefficient with prep time.\n",
    "\n",
    "* The type of food for thai, burgers, italian, and other, all have small coefficients. During th EDA phase there was no obvious variation of prep time between these groups. *type_of_food* indian does a relatively large coefficient, however, suggesting a future iteration of the model could use a binary variable \"Indian food or not indian food\". \n",
    "\n",
    "* The remaining categorical variables also have relatively large coefficients, reflecting the variation we say in their correspondning boxplots. It may be worth adjusting the pooling parameter on the *city* variable, to see if we can acheive some uplift. \n",
    "\n",
    "### Examining the residuals\n",
    "I'll analyse the residuals of the transformed variables, since this is what the model was actually predicting. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "226b2b2a-9650-44d7-91f5-5c11ef3dc2c6",
   "metadata": {},
   "outputs": [],
   "source": [
    "residuals = y_test_trans - predictions_trans"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "462034a5-6490-4d98-8d94-376060359b9a",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(10,4))\n",
    "plt.scatter(predictions_trans, residuals, alpha=0.2)\n",
    "plt.axhline(y=0, c=\"k\", ls=\"--\", lw=1)\n",
    "plt.xlabel(\"$y_{predicted}$\", fontsize=14)\n",
    "plt.ylabel(\"Residual\", fontsize=14)\n",
    "plt.title(\"Linear Regression\")\n",
    "plt.tight_layout()\n",
    "plt.show();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6cb4764-c35e-4cc5-a3a4-ddfe5c4f2a72",
   "metadata": {
    "tags": []
   },
   "source": [
    "The residuals show signs of heteroscedasticity: the variance of the residuals is not constant with $y_{predicted}$. It's possible that this is a symptom of correlation between error terms, arising from the grouped nature of the data. \n",
    "\n",
    "Heteroscedasticity means that the ordinary-least-squares estimate of our linear regression model may not have the smallest sampling variance, or in other words, that the uncertainty surrounding the estimated linear model coefficients my not be as small as they could be. If the effect is severe enough it can invalidate claims about statistical significance, such as the significance of the trend between predictor and target variables. In a real project, I'd investgate solutions to this potential problem.\n",
    "\n",
    "I'll leave it for now, and note that the model coefficients are subject to change upon resampling.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7bab08a-a9dc-4b32-87c9-a442e4c911c7",
   "metadata": {},
   "source": [
    "## Discussion of results\n",
    "The improvement in terms of the RMSE score is small in absolute terms; a linear model improved upon predicting the mean by just 113 seconds. This tells us the model is not capturing a great deal of the variance in the data (approximately 20%).\n",
    "\n",
    "It may be possible to improve upon this score by fine-tuning a random forest model or using a gradient boosting machine, however, I believe there are many sources of variance arising from the grouped nature of the data, which would not be adequately captured by a single model that treats all restaurants equally. For example, other factors that influence the prep time, which vary on an individual restaurant level, could be:\n",
    "* Number of staff working in the kitchen\n",
    "* Experience of staff\n",
    "* The number and complexity of orders taken in a given window before each order in our data set is accepted, some of which will not be visible to Deliveroo (eg telephone orders and walk-ins). \n",
    "\n",
    "Nonetheless, there are some advantages to taking the above approach:\n",
    "* Using a single model for all restaurants means we only have to monitor and maintain a single model in production.\n",
    "* By pooling rare values of categorical variables onto a single value, we are able to predict on new categorical values that were not observed during training, since these values will be mapped onto the rare value. \n",
    "* We can intepret linear model coeficients which can help the business identify driving factors that influence the prep time. \n",
    "\n",
    "In future iterations I would explore two approaches: \n",
    "\n",
    "**Grouping the data and fitting separate models on each group**\n",
    "The idea being that we isolate (to some degree) different sources of variance, so that the variance within each group is less than the entire data set. You could use the available restaurant-level features to define groups or explore clustering methods.\n",
    "\n",
    "Advantages:\n",
    " * We have a set of available features to create groupings, so we can iterate quickly. \n",
    " \n",
    "Disadvantages:\n",
    " * It's possible we could end up with many groups with few data points, making model parameter estimates, and predictions, uncertain.\n",
    " * The more groups we define, the models we must manage in production. \n",
    " * Clustering may not yield meaningful groups, and makes it more difficult to validate the overall pipeline, how do we ensure clusters are robust over time?\n",
    " \n",
    "\\\n",
    "**Hierarchical regression models**.\n",
    "Instead of fitting a single set of regression coefficients for all restaurants, in a hierarchical model we allow each restaurant to have different set of coefficients. The coefficients are assumed to be samples from some latent distribution. \n",
    "\n",
    "Advantages:\n",
    " * They may provide more accurate certain predictions for individual restaurants, including those for which we have only a small number of data points, because we allow model parameters to vary at the the restaurant level, but not completely, since they are assumed to be drawn from the same distribution. \n",
    " * There is a single model to manage in production. \n",
    " \n",
    "Disadvantages:\n",
    " * These models can become difficult to validate and evaluate.\n",
    " * Iteration time can be slow if you choose a Bayesian approach that relies on Monte-Carlo sampling, which can be slow with a large number of data points. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f17980b-5877-4413-8ae8-716c600919bb",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
