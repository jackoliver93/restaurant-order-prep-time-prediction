# Modelling the Preparation Time of Food Orders from Restaurants

## Overview
This repo contains notebooks which describe different approaches to modelling the time taken for restaurants to prepare food orders made by customers. The main focus is on using hierarchical Bayesian regression to capture the fact that orders are grouped by restaurant and their preparation times are unlikely to be independent, and on understanding how this approach compares to more standard modelling approaches that make point estimates while disregarding the grouped structure of the data.

## Data
The data used in this project are contained within two tables:
 * orders: describes individual food orders made by customers.
 * restaurants: information about the restaurants that customers make orders from. 
 
The data for this project is not shared and all notebook cells are cleared before commiting to version control using a pre-commit hook.

## Code
The code is presented as notebooks since this work is mostly experimental and I want to be able to make visualisations next to code. The repo contains the following notebooks:

 * EDA: used to understand the quality of the data, the relationships between variables and the how transformations effect those relationships.
 * DataPrep: creates cleaned, transformed data sets ready for modelling with. The transformed data is saved locally via the bind mount and is not included in version control. This could really be a Python file since there are no visualisations, but for now, it's not.
 * InitialModelling: uses the Scikit-Learn package to test a handful of estimators and obtain a baseline RMSE value. This approach is commonly used to make point estimates of the target variable. 
 * PartialPooling: Numpyro is used to develop a hierarchical Bayesian regression model; time is spent understanding the outputs of this model and comparing it to the point estimates made in the previous notebook.
 
## Other notes
I use Docker to provide a Linux environment, which is a requirement for using the Numpyro package (which relies on JAX). Other options are WSL or Google Colab (or some other cloud service that provides a compute environment). Data is stored as a csv file on my local machine and is made available within the Docker container via a bind mount.

### Commands to build image and run container
In the code below, TAG is the name (the tag) you give to the Docker image which will be built from the Dockerfile; PATH is the path to the Dockerfile used to build the image. Note: the example below would mount everything within the current directy (source="$(pwd)") into the container.

```console
docker build -t TAG PATH
docker run -p 8888:8888 --mount type=bind,source="$(pwd)",target=/usr/src TAG
```